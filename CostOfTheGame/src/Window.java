import java.awt.EventQueue;
import java.awt.Frame;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.AbstractDocument.Content;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.JPanel;
import javax.swing.DropMode;
import java.awt.Font;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextPane;
import java.awt.ComponentOrientation;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridLayout;
import javax.swing.ScrollPaneConstants;

public class Window {

	private static JFrame frame;
	private static JTable table = new JTable();
	private static JTextField txtFiled;
	private static Scanner scanner;
	private static LinkMaker linkMaker = new LinkMaker();
	private static BtnActionListener btnActionListener;
	private static JButton btnNewButton;
	private static TableModel model = new DefaultTableModel();

	public static String getHtml(String link) throws IOException, InterruptedException, URISyntaxException {
		HttpClient client = HttpClient.newHttpClient();
		HttpRequest request = HttpRequest.newBuilder(new URI(link)).build();
		HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
		return response.body().toString();
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Window() throws IOException, InterruptedException, URISyntaxException {
		initialize();

	}

	private void initialize() throws IOException, InterruptedException, URISyntaxException {

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		
		
		
		frame = new JFrame();
		frame.setResizable(true);
		frame.setAlwaysOnTop(true);
		frame.setBackground(new Color(240, 240, 240));
		frame.setBounds(0, 0, 960, 540);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						frame.getContentPane().setLayout(null);
				
						table.setFont(new Font("Times New Roman", Font.PLAIN, 17));
						table.setShowVerticalLines(false);
						table.setShowHorizontalLines(false);
						table.setRowSelectionAllowed(false);
						table.setRowHeight(25);
						table.setIntercellSpacing(new Dimension(0, 0));
						table.setFocusable(false);
						table.setBounds(0, 10, 1900, 865);
						JScrollPane scrollPane = new JScrollPane(table);
						scrollPane.setBounds(10, 11, 924, 381);
						scrollPane.setAlignmentY(1.0f);
						scrollPane.setAlignmentX(1.0f);
						scrollPane.setFont(new Font("Times New Roman", Font.BOLD, 20));
						frame.getContentPane().add(scrollPane);
		
				JPanel panel = new JPanel();
				panel.setBounds(10, 403, 924, 86);
				panel.setLayout(null);
				frame.getContentPane().add(panel);
				
						JLabel lblNewLabel = new JLabel("Data input: ");
						lblNewLabel.setBounds(10, 11, 73, 57);
						lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
						panel.add(lblNewLabel);
						
								txtFiled = new JTextField();
								txtFiled.setBounds(93, 11, 691, 57);
								txtFiled.setFont(new Font("Times New Roman", Font.PLAIN, 17));
								panel.add(txtFiled);
								txtFiled.setColumns(10);
								txtFiled.addKeyListener(new KeyAdapter() {
									public void keyReleased(KeyEvent e) {
										if (e.getKeyCode() == 10) {
											linkMaker.name = txtFiled.getText();
											String html = null;
											try {
												html = getHtml(linkMaker.makeLink(linkMaker.name));
											} catch (IOException e1) {
												e1.printStackTrace();
											} catch (InterruptedException e1) {
												e1.printStackTrace();
											} catch (URISyntaxException e1) {
												e1.printStackTrace();
											}
											Converter converter = new Converter();
											List<Game> games = new ArrayList<Game>();
											games = converter.getGames(html, linkMaker.name);
											String[] headers = { "Name of the game", "link", "Price at $" };
											Object[][] data = new Object[games.size()][3];

											for (int i = 0; i < data.length; i++) {
												Object[] game = new Object[3];
												game[0] = games.get(i).name;
												game[1] = games.get(i).link;
												game[2] = games.get(i).cost;
												data[i] = game;
											}

											model = new DefaultTableModel(data, headers) {
												public Class getColumnClass(int column) {
													Class returnValue;
													if ((column >= 0) && (column < getColumnCount())) {
														returnValue = getValueAt(0, column).getClass();
													} else {
														returnValue = Object.class;
													}
													return returnValue;
												}
											};

											table.setModel(model);
											RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(model);
											table.setRowSorter(sorter);
										}
										;
									}
								});
								// txtpnDataInput.

								JButton btnFind = new JButton("Find");
								btnFind.setBounds(794, 11, 120, 57);
								btnFind.setFont(new Font("Times New Roman", Font.BOLD, 17));
								
										btnFind.addActionListener(new ActionListener() {
								
											public void actionPerformed(ActionEvent e) {
												linkMaker.name = txtFiled.getText();
												String html = null;
												try {
													html = getHtml(linkMaker.makeLink(linkMaker.name));
												} catch (IOException e1) {
													e1.printStackTrace();
												} catch (InterruptedException e1) {
													e1.printStackTrace();
												} catch (URISyntaxException e1) {
													e1.printStackTrace();
												}
												Converter converter = new Converter();
												List<Game> games = new ArrayList<Game>();
												games = converter.getGames(html, linkMaker.name);
												String[] headers = { "Name of the game", "link", "Price at $" };
												Object[][] data = new Object[games.size()][3];
								
												for (int i = 0; i < data.length; i++) {
													Object[] game = new Object[3];
													game[0] = games.get(i).name;
													game[1] = games.get(i).link;
													game[2] = games.get(i).cost;
													data[i] = game;
												}
								
												model = new DefaultTableModel(data, headers) {
													public Class getColumnClass(int column) {
														Class returnValue;
														if ((column >= 0) && (column < getColumnCount())) {
															returnValue = getValueAt(0, column).getClass();
														} else {
															returnValue = Object.class;
														}
														return returnValue;
													}
												};
								
												table.setModel(model);
												RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(model);
												table.setRowSorter(sorter);
											}
										});
										panel.add(btnFind);
		frame.setVisible(true);

	}
}
