import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Converter {

	public Converter() {
	}

	public String getName(Element element) {
		return element.getElementsByClass("title").text();
	}

	public Boolean ifNameEquals(String nameOnTheSite, String nameOfTheGame) {
		if (nameOnTheSite.toLowerCase().replaceAll("\\s+", "")
				.contains(nameOfTheGame.toLowerCase().replaceAll("\\s+", ""))) {
			return true;
		}
		return false;

	}

	public String getLink(Element element, String patternGotHtml, String patterGotLink) {
		Pattern pattern = Pattern.compile(patternGotHtml, Pattern.DOTALL);
		Matcher matcher = pattern.matcher(element.toString());
		matcher.find();
		pattern = Pattern.compile(patterGotLink, Pattern.DOTALL);
		matcher = pattern.matcher(matcher.group(0));
		matcher.find();
		return matcher.group(0).replaceAll("\"", "");
	}

	public double getCost(Element elementGot, String patternGotHtml, String patternGotCost) {
		Element element = elementGot;
		Pattern pattern = Pattern.compile(patternGotHtml, Pattern.DOTALL);
		Matcher matcher = pattern.matcher(element.toString());

		if (matcher.find()) {
			pattern = Pattern.compile(patternGotCost, Pattern.DOTALL);
			matcher = pattern.matcher(matcher.group(0));
			matcher.find();
			String cost = matcher.group(0).replaceAll("\"", "");
			if (Integer.parseInt(cost) == 0) {
				cost = "0.00";
				return Double.parseDouble(cost);
			}
			if (cost.length() == 1) {

				char[] charArrayCost = new char[cost.length() + 3];

				charArrayCost[charArrayCost.length - 1] = cost.charAt(0);
				charArrayCost[charArrayCost.length - 2] = '0';
				charArrayCost[charArrayCost.length - 3] = '.';
				charArrayCost[charArrayCost.length - 4] = '0';
				cost = charArrayCost.toString();
				cost = String.valueOf(charArrayCost);

				return Double.parseDouble(cost);
			}
			if (cost.length() == 2) {
				char[] charArrayCost = new char[cost.length() + 2];

				for (int j = 0; j < charArrayCost.length - 2; j++) {
					charArrayCost[j] = cost.charAt(j);
				}

				charArrayCost[charArrayCost.length - 1] = charArrayCost[charArrayCost.length - 3];
				charArrayCost[charArrayCost.length - 2] = charArrayCost[charArrayCost.length - 4];
				charArrayCost[charArrayCost.length - 3] = '.';
				charArrayCost[charArrayCost.length - 4] = '0';

				cost = charArrayCost.toString();

				cost = String.valueOf(charArrayCost);

				return Double.parseDouble(cost);

			} else {
				char[] charArrayCost = new char[cost.length() + 1];
				for (int j = 0; j < charArrayCost.length - 1; j++) {
					charArrayCost[j] = cost.charAt(j);
				}
				charArrayCost[charArrayCost.length - 1] = charArrayCost[charArrayCost.length - 2];
				charArrayCost[charArrayCost.length - 2] = charArrayCost[charArrayCost.length - 3];
				charArrayCost[charArrayCost.length - 3] = '.';
				cost = charArrayCost.toString();
				cost = String.valueOf(charArrayCost);
				return Double.parseDouble(cost);
			}
		}
		return 0;
	}

	public ArrayList<Game> getGames(String html, String nameOfTheGame) {
		Document document = Jsoup.parse(html);
		Elements elements = document.getElementsByClass("search_result_row ds_collapse_flag ");
		Element elementName = null;
		ArrayList<Game> games = new ArrayList<Game>();
		Converter converter = new Converter();
		int i = 0;

		for (Element element : elements) {
			if (converter.ifNameEquals(converter.getName(element), nameOfTheGame)) {
				games.add(i,
						new Game(converter.getName(element),
								(converter.getCost(element, "data-price-final=\"(.*?)\"", "\"(.*?)\"")),
								converter.getLink(element, "href=\"(.*?)\"", "\"(.*?)\"")));
				i++;
			}
		}
		return games;
	}

}
