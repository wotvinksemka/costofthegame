
public class Game {

	public String name;
	public Double cost;
	public String link;

	public Game(String name, Double cost, String link) {
		super();
		this.name = name;
		this.cost = cost;
		this.link = link;
	}

	@Override
	public String toString() {
		return "Game [name=" + name + ", cost=" + cost + "]" + "\n";
	}

}
